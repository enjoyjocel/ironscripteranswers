﻿Function Get-Netstat{

#$netstatRes = netstat -a
$NetStatHolder = @()

$netstatRes[4..($netstatRes.Length)] | % {$_.Trim()} | ConvertFrom-String | %{
    $stat = New-Object -TypeName PSobject -Property @{
       "Protocol" = $_.p1
       "Local Address" = $_.p2
       "Foreign Address" = $_.p3
       "State" = $_.p4
    }

    $netstatHolder += $stat
}

$NetStatHolder

}

function Get-ArpA {
    
    $arpa = arp -a | ConvertFrom-String
    $Inter_index = @()
    $Index = 0
    $arpa_res = @()
    $arpa | % {
        if($_.p1 -like "*Interface*")
        {
            $inter_row = new-object -TypeName PSObject -Property @{
                "Index" = $index
                "Interface IP" = $_.p2
            }
       
            $Inter_index += $inter_row
        }
        $Index++
    }

    $count = 0
    foreach($ent in $inter_index){
        $start = $Inter_index[$count].index + 2
    
        if($count -eq ($Inter_index.Length - 1)){
            $end = $arpa.Length
        }
    
        else{
            $end = $Inter_index[($count+1)].index - 1
        }

        for($i= $start;$i -le $end;$i++){
        
            $row = New-Object -TypeName PSobject -Property @{
                "Interface" = $ent."Interface IP"
                "Internet Address" = $arpa[$i].p2
                "MAC Address" = $arpa[$i].p3
                "Type" = $arpa[$i].p4
        
            }
            $arpa_res += $row   
        }
    }

    $arpa_res

}
Get-ArpA
Get-Netstat