﻿Function Get-OSInfo{

    [CmdletBinding()]
    param(
    [Parameter(Mandatory)]
    [String]$RemoteHost

    )


    $objOperatingSystem = Get-WmiObject -ComputerName $RemoteHost -Query "SELECT * FROM Win32_OperatingSystem"
    $disks = Get-WmiObject -ComputerName $RemoteHost -Query "SELECT * FROM Win32_LogicalDisk"

    $DisksInf = @()

    Write-Verbose "Extracting Disk INformation"

    foreach ($objDisk in $disks) { 

        $DiskInfo = New-Object -TypeName PSObject 
        $DiskInfo | Add-Member -MemberType NoteProperty -Name "Drive" -Value $objDisk.DeviceID
        $Diskinfo | Add-Member -MemberType NoteProperty -Name "Drive Type" -Value $objDisk.Description
        $Diskinfo | Add-Member -MemberType NoteProperty -Name "Size" -Value "$([math]::Round(($objDisk.Size / 1GB),2)) GB"
        $diskinfo | Add-Member -MemberType NoteProperty -Name "Free Space" -Value "$([math]::Round(($objDisk.FreeSpace / 1GB),2)) GB"
        $diskinfo | Add-Member -MemberType NoteProperty -Name "Percent Used" -Value "$([math]::Round(100 - ($objDisk.FreeSpace / $objDisk.Size)*100)) %"
        $diskinfo | Add-Member -MemberType NoteProperty -Name "Compressed" -Value "$([math]::Round(($objDisk.Compressed / 1GB),2)) GB"
        $DisksInf += $diskInfo

        Write-Debug -Message "Current value: $DisksInf"
     
    }
    
    $OsInfo = New-Object -TypeName Psobject

    $osinfo | Add-Member -MemberType NoteProperty -Name "OS Name" -Value (($objOperatingSystem.Name).Substring(0,(($objOperatingSystem.Name).IndexOf("|"))))
    $osinfo | Add-Member -MemberType NoteProperty -Name "Version" -Value $objOperatingSystem.Version
    $OsInfo | Add-Member -MemberType NoteProperty -Name "Service Pack" -Value "$($objOperatingSystem.ServicePackMajorVersion).$($objOperatingSystem.ServicePackMinorVersion)"
    $OsInfo | Add-Member -MemberType NoteProperty -Name "OS Manufacturer" -Value $objOperatingSystem.Manufacturer
    $osinfo | Add-Member -MemberType NoteProperty -Name "Windows Directory" -Value $objOperatingSystem.WindowsDirectory
    $osinfo | Add-Member -MemberType NoteProperty -Name "Locale" -Value $objOperatingSystem.Locale
    $osinfo | Add-Member -MemberType NoteProperty -Name "Available Physical Memory" -Value "$([math]::Round(($objOperatingSystem.FreePhysicalMemory / 1MB),2)) GB"
    $osinfo | Add-Member -MemberType NoteProperty -Name "Total virtual Memory" -Value "$([math]::Round(($objOperatingSystem.TotalVirtualMemorySize / 1MB),2)) GB"
    $osinfo | Add-Member -MemberType NoteProperty -Name "Available Virtual Memory" -Value "$([math]::Round(($objOperatingSystem.FreeVirtualMemory / 1MB),2)) GB"
    $OsInfo | Add-Member -MemberType NoteProperty -Name "Disk Information" -Value $DisksInf
    $osinfo
}

$res = Get-OSInfo -RemoteHost Localhost -Verbose -Debug
$res.'Disk Information'