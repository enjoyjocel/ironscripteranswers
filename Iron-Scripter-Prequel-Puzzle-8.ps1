﻿$word = Read-Host -Prompt "What is the password?" -AsSecureString

New-LocalUser -FullName "Bill Bennsson" -AccountNeverExpires -Name "B.bennsson" -Password $word 
New-LocalUser -FullName "Andy Pandien" -AccountNeverExpires -Name "A.Pandien" -Password $word
New-Item -Path c:\ -Name "SpecialFolder" -ItemType Directory
New-Item -Path c:\SpecialFolder -Name "SpecialFile" -ItemType File
get-process | Out-File -FilePath C:\SpecialFolder\SpecialFile.txt

$acl = get-acl -path C:\SpecialFolder\SpecialFile.txt
$user = Get-LocalUser | ? {$_.name -eq "B.Bennsson"}

$perm = New-Object System.Security.AccessControl.FileSystemAccessRule("$($user.Name)","Read","Allow")
$acl.AddAccessRule($perm)

set-acl -Path C:\SpecialFolder\SpecialFile.txt -AclObject $acl