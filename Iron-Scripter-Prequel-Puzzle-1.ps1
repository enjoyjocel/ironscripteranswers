﻿Function Get-MonitorInfo{

    [CmdletBinding()]
    Param(
    [Parameter(Mandatory=$true,ValueFromPipeline,Position=1)]
    [String]$RemoteHost
    )

    Try{

        $Computer = Get-WmiObject -ComputerName $RemoteHost -Class Win32_ComputerSystem -ErrorAction Stop
    }
    Catch{
        Throw "There is an error connecting to the host to get the computer properties"
    }

    Try{
            $Monitor = Get-WmiObject -ComputerName $RemoteHost wmiMonitorID -namespace root\wmi -ErrorAction Stop

            $Monitor | % { 

                $psObject = New-Object -TypeName PSObject

                $psObject | Add-Member NoteProperty -name ComputerName -value $Computer.PSComputerName
                $psObject | Add-Member NoteProperty -name ComputerType -value $Computer.Model
                $psObject | Add-Member NoteProperty -name Memory -value "$([math]::round(($Computer.TotalPhysicalMemory / 1GB),2)) GB"
                $psObject | Add-Member NoteProperty -name MonitorSerial -value ($_.SerialNumberID -join "") 
                $psObject | Add-Member NoteProperty -name MonitorType -value ""
                
                if(!($_.UserFriendlyName -eq $null)){
                    
                    $psObject.MonitorType = ($_.UserFriendlyName -ne 0 | % {[char]$_}) -join ""
                }

                $psObject 
            }
    }

    Catch{

        Throw "There is an error connecting to the host to get the monitor properties"
    
    }
}
$env:Computername | Get-MonitorInfo
