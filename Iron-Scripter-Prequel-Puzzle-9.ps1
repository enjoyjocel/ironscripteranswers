﻿Function Get-FolderINfo{
    param(
        $Path = "C:\Users\enjoy\OneDrive\Ipsoft\PSScripts"
    )

    $lengthc = 0
    $script:Res = Get-ChildItem $path -Recurse | select name,@{name="size";e={$_.Length;}},@{name="type";E={if($_.Attributes -like "*Directory*"){"Folder"}else{"File"}}},LastWriteTime
    $Res.size | % {$lengthc += $_}
    $script:DirInfo = New-Object -TypeName PSObject -Property @{
    
        "Number of Folders" = ([array]($Res | ? {$_.Type -eq "Folder"})).length
        "Number of Files" = ($Res | ? {$_.Type -eq "File"}).length
        "Sum Size" = "$([math]::Round(($lengthc) / 1mb,2)) Mb"
    }


}

Get-FolderINfo C:\Users\enjoy\AppData\Local\Temp





    


