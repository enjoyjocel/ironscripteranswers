﻿Function Read-Feed {

    [CmdletBinding()]
    Param(
        $URL
    )

    [Int]$index = 1
    $res = Invoke-restmethod -Uri $URL | select Title, @{Name="PUblication Date";E={$_.PubDate}},Link,@{Name="Author";E={$_.Creator."#cdata-section"}} 

    $Holder = @()
    foreach($item in $res){
        $addhold = $item | Select @{Name="Index";E={"$index"}},*

        $index++
        $holder += $addhold
    }

    $Holder | ft

    $choice = Read-Host -Prompt "Write The Index of the link you want to read"
    $tonav = ($Holder | ? {$_.index -eq $choice}).link

    $ie = New-Object -ComObject "InternetExplorer.Application"

    $ie.Visible = $true
    $ie.Navigate($tonav)

}

Read-Feed -URL https://blogs.msdn.microsoft.com/powershell/feed/







