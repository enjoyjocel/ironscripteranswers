﻿function Get-Perfcount {
    
    $Perproctime = gwmi -ClassName Win32_PerfFormattedData_Counters_ProcessorInformation | ? {$_.name -eq "_Total"} | select  -ExpandProperty PercentProcessorTime
    $diskc = gwmi Win32_PerfFormattedData_PerfDisk_LogicalDisk | ? {$_.name -eq "C:"} | select -ExpandProperty PercentFreeSpace
    $permem = gwmi Win32_PerfFormattedData_PerfOS_Memory | select -ExpandProperty PercentCommittedBytesInUse
    $adapter = gwmi Win32_PerfFormattedData_Tcpip_NetworkAdapter |  select name, BytesTotalPersec | Sort-Object -Descending -Property BytesTotalPersec | select * -First 2

    $perfcount = New-Object -TypeName PSobject -Property @{
        "Processor" = $Perproctime
        "Disk C" = $diskc
        "Memory" = $permem
        "Adapter" = $adapter
    }

    write-output $perfcount

}

Function get-counterInfo {

    $counters = @(
        '\Processor(*)\% Processor Time'
        '\LogicalDisk(C:)\% Free Space'
        '\Memory\% Committed Bytes In Use'
        '\network adapter(intel[r] dual band wireless-ac 7260)\bytes total/sec'
    )

Get-Counter -Counter $counters | select -ExpandProperty CounterSamples
}

